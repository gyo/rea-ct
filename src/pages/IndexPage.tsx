import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { Link } from "react-router-dom";

const Component: React.FC<RouteComponentProps> = () => {
  return (
    <>
      <Helmet>
        <title>Index</title>
      </Helmet>

      <ul>
        <li>
          <Link to="/rsv-home/">RsvHome</Link>
        </li>
        <li>
          <Link to="/tic-tac-toe/">TicTacToePage</Link>
        </li>
        <li>
          <Link to="/introducing-hooks/">IntroducingHooks</Link>
        </li>
        <li>
          <Link to="/introducing-hooks-2/">IntroducingHooks2</Link>
        </li>
        <li>
          <Link to="/hooks-as-redux/">HooksAsRedux</Link>
        </li>
      </ul>
    </>
  );
};

export default withRouter(Component);
