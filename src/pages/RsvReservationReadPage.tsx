import EditIcon from "@material-ui/icons/Edit";
import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import FabLink from "../components/FabLink";
import RsvAppShell from "../components/RsvAppShell/";
import RsvReservationForm from "../containers/RsvReservationForm/Reader";

export type IRsvReservationPageProps = RouteComponentProps<{
  reservationId: string;
}>;

const Component: React.FC<IRsvReservationPageProps> = ({ match, location }) => {
  const reservationId = match.params.reservationId;

  return (
    <>
      <Helmet>
        <title>予約</title>
      </Helmet>

      <RsvAppShell title="予約" pathname={location.pathname}>
        <RsvReservationForm mode="viewer" reservationId={reservationId} />

        <div style={{ position: "fixed", right: 16, bottom: 16 }}>
          <FabLink
            to={`/rsv-reservations/${reservationId}/update/`}
            aria-label="編集"
          >
            <EditIcon />
          </FabLink>
        </div>
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
