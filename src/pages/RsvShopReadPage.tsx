import EditIcon from "@material-ui/icons/Edit";
import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import FabLink from "../components/FabLink";
import RsvAppShell from "../components/RsvAppShell/";
import RsvShopForm from "../containers/RsvShopForm/Reader";

export type IRsvShopPageProps = RouteComponentProps<{ shopId: string }>;

const Component: React.FC<IRsvShopPageProps> = ({ match, location }) => {
  const shopId = match.params.shopId;

  return (
    <>
      <Helmet>
        <title>店舗</title>
      </Helmet>

      <RsvAppShell title="店舗" pathname={location.pathname}>
        <RsvShopForm mode="viewer" shopId={shopId} />

        <div style={{ position: "fixed", right: 16, bottom: 16 }}>
          <FabLink to={`/rsv-shops/${shopId}/update/`} aria-label="編集">
            <EditIcon />
          </FabLink>
        </div>
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
