import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { useCounter } from "../hooks/counter";

const Component: React.FC<RouteComponentProps> = () => {
  // console.log は update のたびに呼ばれるが、 useState は最初のレンダリング時にのみ呼ばれる。
  console.log("I am a component.");
  const [count, setCount] = useCounter();
  const [count2, setCount2] = useCounter();
  const [age, setAge] = React.useState(() => {
    const today = new Date();
    const birthday = new Date(1990, 1, 1);
    const age = today.getFullYear() - birthday.getFullYear();
    return age;
  });
  const [todos] = React.useState(["hoge", "fuga"]);

  return (
    <>
      {/* react-helmet と useEffect を一緒に使うときは、現時点ではこのように書かないといけない */}
      {/* React-helmet crashes with the hook useEffect · Issue #437 · nfl/react-helmet https://github.com/nfl/react-helmet/issues/437 */}
      <Helmet title="Introducing Hooks" />

      <h1>Introducing Hooks</h1>

      <div>
        <h2>リンク</h2>

        <a
          href="https://reactjs.org/docs/hooks-intro.html"
          target="_black"
          rel="noreferrer noopener"
        >
          Introducing Hooks
        </a>
      </div>

      <div>
        <h2>Count</h2>
        <p>You clicked {count} times.</p>
        <button onClick={() => setCount(count + 1)}>COUNT UP</button>
      </div>

      <div>
        <h2>Count 2</h2>
        <p>You clicked {count2} times.</p>
        <button onClick={() => setCount2(count2 => count2 + 1)}>
          COUNT UP
        </button>
      </div>

      <div>
        <h2>Age</h2>
        <p>I am {age} years old.</p>
        <button onClick={() => setAge(age + 1)}>COUNT UP</button>
      </div>

      <div>
        <h2>Todos</h2>
        <ul>
          {todos.map((todo, index) => (
            <li key={index}>{todo}</li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default withRouter(Component);
