import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import HooksAsReduxChild1 from "../components/HooksAsReduxChild1";
import HooksAsReduxChild2 from "../components/HooksAsReduxChild2";
import Context from "../hooksRedux/context";
import reducer from "../hooksRedux/reducer";
import * as store from "../hooksRedux/store";

const Component: React.FC<RouteComponentProps> = () => {
  const [state, dispatch] = React.useReducer(reducer, store.initialState);

  return (
    <>
      <Helmet title="Hooks Redux" />

      <h1>Hooks Redux</h1>

      <a
        href="https://qiita.com/terrierscript/items/1516e946dfe91397c229"
        target="_blank"
        rel="noopener noreferrer"
      >
        React Hooksでredux / react-reduxでやってたことを色々やってみる - Qiita
      </a>

      <hr />

      <Context.Provider value={{ state, dispatch }}>
        <HooksAsReduxChild1 />
        <hr />
        <HooksAsReduxChild2 />
        <hr />
      </Context.Provider>
    </>
  );
};

export default withRouter(Component);
