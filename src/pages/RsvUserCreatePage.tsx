import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvUserForm from "../containers/RsvUserForm/Creator";

const Component: React.FC<RouteComponentProps> = ({ history, location }) => {
  return (
    <>
      <Helmet>
        <title>ユーザー新規作成</title>
      </Helmet>

      <RsvAppShell title="ユーザー新規作成" pathname={location.pathname}>
        <RsvUserForm
          history={history}
          transitionTo={`/rsv-users/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
