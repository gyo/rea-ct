import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { useCounter } from "../hooks/counter";

const Component: React.FC<RouteComponentProps> = () => {
  const [count, setCount] = useCounter();

  return (
    <>
      <Helmet title="Introducing Hooks 2" />

      <h1>Introducing Hooks 2</h1>

      <div>
        <h2>Counter!!!!!</h2>

        <p>{count}!!!!!</p>

        <button onClick={() => setCount(count + 1)}>COUNT UP 1</button>
        <button onClick={() => setCount(count + 2)}>COUNT UP 2</button>
        <button onClick={() => setCount(count + 3)}>COUNT UP 3</button>
        <button onClick={() => setCount(count + 4)}>COUNT UP 4</button>
      </div>
    </>
  );
};

export default withRouter(Component);
