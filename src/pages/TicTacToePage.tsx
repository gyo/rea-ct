import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";

type SquareIndex = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8;
type Square = "x" | "o" | "_";
type Squares = Square[];

const useToggle = (initialState: boolean = false): [boolean, () => void] => {
  const [state, setState] = React.useState(initialState);
  const toggle = () => setState(s => !s);
  return [state, toggle];
};

const calculateWinner = (squares: Squares): "x" | "o" | null => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (const line of lines) {
    const [a, b, c] = line;
    const squareA = squares[a];
    if (squareA !== "_" && squareA === squares[b] && squareA === squares[c]) {
      return squareA;
    }
  }
  return null;
};

const getStatusMessage = (squares: Squares, xIsNext: boolean) => {
  console.log("getStatusMessage called.");
  const winner = calculateWinner(squares);
  return winner != null
    ? `Winner is ${winner}`
    : squares.every(s => s !== "_")
    ? "Draw"
    : `Next player is ${xIsNext ? "x" : "o"}`;
};

const Board = () => {
  const initialSquares: Squares = ["_", "_", "_", "_", "_", "_", "_", "_", "_"];
  const [squares, setSquares]: [
    Squares,
    React.Dispatch<React.SetStateAction<Squares>>
  ] = React.useState(initialSquares);
  const [xIsNext, toggleXIsNext] = useToggle(true);

  const statusMessage = getStatusMessage(squares, xIsNext);

  const renderSquare = (squareIndex: SquareIndex) => {
    return (
      <button className="square" onClick={() => selectSquare(squareIndex)}>
        {squares[squareIndex]}
      </button>
    );
  };

  const selectSquare = (squareIndex: SquareIndex) => {
    const winner = calculateWinner(squares);
    if (winner != null || squares[squareIndex] !== "_") {
      return;
    }
    setSquares(s => {
      const squareCopy = [...s];
      squareCopy[squareIndex] = xIsNext ? "x" : "o";
      return squareCopy;
    });
    toggleXIsNext();
  };

  return (
    <div>
      <div className="status">{statusMessage}</div>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
};

const Game = () => {
  return (
    <div className="game">
      <div className="game-board">
        <Board />
      </div>
    </div>
  );
};

const Component: React.FC<RouteComponentProps> = () => {
  return (
    <>
      <Helmet>
        <title>Tic-Tac-Toe</title>
      </Helmet>

      <h1>Tic-Tac-Toe with react Hooks</h1>

      <Game />
    </>
  );
};

export default withRouter(Component);
