import { reducerWithInitialState } from "typescript-fsa-reducers";
import * as action from "../actions/";
import * as store from "../stores/";

const appReducer = reducerWithInitialState(store.initialState)
  .case(action.createMenu, (state, payload) => {
    return {
      ...state,
      menus: [
        ...state.menus,
        {
          ...payload.menu,
          id: `${Date.now()}-${Math.random()}`
        }
      ]
    };
  })
  .case(action.updateMenu, (state, payload) => {
    const index: number = state.menus.findIndex(
      menu => menu.id === payload.menu.id
    );

    if (index === -1) {
      return { ...state };
    }

    return {
      ...state,
      menus: [
        ...state.menus.slice(0, index),
        {
          ...payload.menu
        },
        ...state.menus.slice(index + 1)
      ]
    };
  })
  .case(action.createReservation, (state, payload) => {
    return {
      ...state,
      reservations: [
        ...state.reservations,
        {
          ...payload.reservation,
          id: `${Date.now()}-${Math.random()}`
        }
      ]
    };
  })
  .case(action.updateReservation, (state, payload) => {
    const index: number = state.reservations.findIndex(
      reservation => reservation.id === payload.reservation.id
    );

    if (index === -1) {
      return { ...state };
    }

    return {
      ...state,
      reservations: [
        ...state.reservations.slice(0, index),
        {
          ...payload.reservation
        },
        ...state.reservations.slice(index + 1)
      ]
    };
  })
  .case(action.createShop, (state, payload) => {
    return {
      ...state,
      shops: [
        ...state.shops,
        {
          ...payload.shop,
          id: `${Date.now()}-${Math.random()}`
        }
      ]
    };
  })
  .case(action.updateShop, (state, payload) => {
    const index: number = state.shops.findIndex(
      shop => shop.id === payload.shop.id
    );

    if (index === -1) {
      return { ...state };
    }

    return {
      ...state,
      shops: [
        ...state.shops.slice(0, index),
        {
          ...payload.shop
        },
        ...state.shops.slice(index + 1)
      ]
    };
  })
  .case(action.createUser, (state, payload) => {
    return {
      ...state,
      users: [
        ...state.users,
        {
          ...payload.user,
          id: `${Date.now()}-${Math.random()}`
        }
      ]
    };
  })
  .case(action.updateUser, (state, payload) => {
    const index: number = state.users.findIndex(
      user => user.id === payload.user.id
    );

    if (index === -1) {
      return { ...state };
    }

    return {
      ...state,
      users: [
        ...state.users.slice(0, index),
        {
          ...payload.user
        },
        ...state.users.slice(index + 1)
      ]
    };
  });

export default appReducer;
