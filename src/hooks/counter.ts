import * as React from "react";

export const useCounter = (): [
  number,
  React.Dispatch<React.SetStateAction<number>>
] => {
  console.log("useCounter Hook called");
  const [count, setCount] = React.useState(0);

  React.useEffect(() => {
    // useEffect は、最初のレンダリングも含めてレンダリングのたびに呼ばれる。
    console.log(`useEffect Hook called. conut: ${count}`);

    return () => {
      console.log(`useEffect Hook clean-up function called. conut: ${count}`);
    };
  }, [count]); // コンポーネントが re-render されるたびに全ての `useEffect` が発火するのを防ぐ

  return [count, setCount];
};
