import * as React from "react";
import { dateTimeLocalToDate } from "../utilities/";

export const useFormItem = <T>(
  initialValue: T
): [T, React.Dispatch<React.SetStateAction<T>>] => {
  const [value, setValue] = React.useState(initialValue);

  return [value, setValue];
};

export const useTextField = (
  initialValue: string
): [string, (e: React.ChangeEvent<HTMLInputElement>) => void] => {
  const [value, setValue] = React.useState(initialValue);

  const updateValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  return [value, updateValue];
};

export const useNumberField = (
  initialValue: number
): [number, (e: React.ChangeEvent<HTMLInputElement>) => void] => {
  const [value, setValue] = React.useState(initialValue);

  const updateValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    const parsedValue = parseInt(e.target.value, 10);
    setValue(isNaN(parsedValue) ? 0 : parsedValue);
  };

  return [value, updateValue];
};

export const useDateField = (
  initialValue: Date
): [Date, (e: React.ChangeEvent<HTMLInputElement>) => void] => {
  const [value, setValue] = React.useState(initialValue);

  const updateValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(dateTimeLocalToDate(e.target.value));
  };

  return [value, updateValue];
};
