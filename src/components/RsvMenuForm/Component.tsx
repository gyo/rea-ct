import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { History } from "history";
import * as React from "react";
import { useTextField, useNumberField } from "../../hooks";
import { dateTimeLocalToDate } from "../../utilities";
import { IMenu } from "../../stores/";
import { IShop } from "../../stores/";
import ReservableDateTimeList, {
  IRsvReservableDateTimeListProps
} from "./ReservableDateTimeList";

export type IRsvMenuFormProps = IRsvReservableDateTimeListProps & {
  menuId?: string;
  menu?: IMenu;
  shops?: IShop[];
  mode?: "viewer" | "editor";
  submit?: (menu: IMenu) => void;
  submitText?: string;
  history?: History;
  transitionTo?: string;
};

const Component: React.FC<IRsvMenuFormProps> = ({
  menuId,
  menu,
  mode = "editor",
  shops = [],
  submit = () => {},
  submitText = "SUBMIT",
  history,
  transitionTo
}) => {
  const [name, updateName] = useTextField(menu ? menu.name : "");

  const [requiredMinutes, updateRequiredMinutes] = useNumberField(
    menu ? menu.requiredMinutes : 0
  );

  const [reservableDateTimes, setReservableDateTime]: [
    IMenu["reservableDateTimes"],
    React.Dispatch<React.SetStateAction<IMenu["reservableDateTimes"]>>
  ] = React.useState(menu ? menu.reservableDateTimes : []);
  const addReservableDateTime = () => {
    setReservableDateTime([
      ...reservableDateTimes,
      {
        endTime: new Date(),
        startTime: new Date()
      }
    ]);
  };
  const updateReservableDateTime = (
    e: React.ChangeEvent<HTMLInputElement>,
    listIndex: number,
    startOrEnd: "start" | "end"
  ) => {
    setReservableDateTime([
      ...reservableDateTimes.slice(0, listIndex),
      {
        ...reservableDateTimes[listIndex],
        [`${startOrEnd}Time`]: dateTimeLocalToDate(e.target.value)
      },
      ...reservableDateTimes.slice(listIndex + 1)
    ]);
  };
  const deleteReservableDateTime = (listIndex: number) => {
    setReservableDateTime([
      ...reservableDateTimes.slice(0, listIndex),
      ...reservableDateTimes.slice(listIndex + 1)
    ]);
  };

  const [resourceOccupancy, updateResourceOccupancy] = useNumberField(
    menu ? menu.resourceOccupancy : 0
  );

  const [shopId, updateShopId] = useTextField(menu ? menu.shopId : "");

  const isEditor = mode === "editor";

  return menuId && !menu ? (
    <div>対象メニューが存在しません。</div>
  ) : (
    <>
      <div style={{ padding: 16 }}>
        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField label="名前" value={name} onChange={updateName} />
          ) : (
            <>
              <Typography variant="caption" component="label">
                名前
              </Typography>

              <Typography>{menu ? menu.name : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              label="所要時間"
              value={requiredMinutes}
              onChange={updateRequiredMinutes}
            />
          ) : (
            <>
              <Typography variant="caption" component="label">
                所要時間
              </Typography>

              <Typography>{menu ? menu.requiredMinutes : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          <Typography variant="caption" component="label">
            予約可能日時
          </Typography>

          <div>
            <ReservableDateTimeList
              mode={isEditor ? "editor" : "viewer"}
              reservableDateTimes={
                isEditor
                  ? reservableDateTimes
                  : menu
                  ? menu.reservableDateTimes
                  : []
              }
              addReservableDateTime={addReservableDateTime}
              updateReservableDateTime={updateReservableDateTime}
              deleteReservableDateTime={deleteReservableDateTime}
            />
          </div>
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              label="リソース占有率"
              value={resourceOccupancy}
              onChange={updateResourceOccupancy}
            />
          ) : (
            <>
              <Typography variant="caption" component="label">
                リソース占有率
              </Typography>

              <Typography>{menu ? menu.resourceOccupancy : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              select={true}
              label="店舗ID"
              value={shopId}
              onChange={updateShopId}
            >
              {shops.map(shop => (
                <MenuItem key={shop.id} value={shop.id}>
                  {shop.name}
                </MenuItem>
              ))}
            </TextField>
          ) : (
            <>
              <Typography variant="caption" component="label">
                店舗ID
              </Typography>

              <Typography>{menu ? menu.shopId : ""}</Typography>
            </>
          )}
        </div>

        {isEditor && (
          <div style={{ marginTop: 16 }}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              onClick={() => {
                submit({
                  id: menuId || "",
                  name,
                  requiredMinutes,
                  reservableDateTimes,
                  resourceOccupancy,
                  shopId
                });
                if (history && transitionTo) {
                  history.push(transitionTo);
                }
              }}
            >
              {submitText}
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default Component;
