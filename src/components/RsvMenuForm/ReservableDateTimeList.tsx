import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import * as React from "react";
import { dateTimeLocalToString } from "../../utilities";
import { IReservableDateTime } from "../../stores/";

export interface IRsvReservableDateTimeListProps {
  mode?: "viewer" | "editor";
  reservableDateTimes?: IReservableDateTime[];
  addReservableDateTime?: () => void;
  updateReservableDateTime?: (
    e: React.ChangeEvent<HTMLInputElement>,
    listIndex: number,
    startOrEnd: "start" | "end"
  ) => void;
  deleteReservableDateTime?: (listIndex: number) => void;
}

const Component: React.FC<IRsvReservableDateTimeListProps> = ({
  mode = "editor",
  reservableDateTimes = [],
  addReservableDateTime = () => {},
  updateReservableDateTime = () => () => {},
  deleteReservableDateTime = () => {}
}) => {
  const isEditor = mode === "editor";

  return (
    <>
      {reservableDateTimes.length > 0 ? (
        <>
          <div style={{ paddingTop: 8, paddingLeft: 8, paddingRight: 8 }}>
            <Divider />
          </div>

          {reservableDateTimes.map((reservableDateTime, index) => (
            <div
              style={{ paddingTop: 8, paddingLeft: 8, paddingRight: 8 }}
              key={index}
            >
              <Grid container={true} alignItems="center">
                <Grid item={true} xs={true}>
                  <div style={{ padding: 8 }}>
                    {isEditor ? (
                      <TextField
                        type="datetime-local"
                        label="開始時間"
                        value={dateTimeLocalToString(
                          reservableDateTime.startTime
                        )}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                          updateReservableDateTime(e, index, "start");
                        }}
                      />
                    ) : (
                      <>
                        <Typography variant="caption" component="label">
                          開始時間
                        </Typography>

                        <Typography>
                          {dateTimeLocalToString(reservableDateTime.startTime)}
                        </Typography>
                      </>
                    )}
                  </div>

                  <div style={{ padding: 8 }}>
                    {isEditor ? (
                      <TextField
                        type="datetime-local"
                        label="終了時間"
                        value={dateTimeLocalToString(
                          reservableDateTime.endTime
                        )}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                          updateReservableDateTime(e, index, "end");
                        }}
                      />
                    ) : (
                      <>
                        <Typography variant="caption" component="label">
                          終了時間
                        </Typography>

                        <Typography>
                          {dateTimeLocalToString(reservableDateTime.endTime)}
                        </Typography>
                      </>
                    )}
                  </div>
                </Grid>

                {isEditor && (
                  <Grid item={true} style={{ padding: 8 }}>
                    <IconButton onClick={() => deleteReservableDateTime(index)}>
                      <DeleteIcon />
                    </IconButton>
                  </Grid>
                )}
              </Grid>

              <div style={{ paddingTop: 8 }}>
                <Divider />
              </div>
            </div>
          ))}
        </>
      ) : (
        <div>List is empty.</div>
      )}

      {isEditor && (
        <IconButton onClick={addReservableDateTime}>
          <AddIcon />
        </IconButton>
      )}
    </>
  );
};

export default Component;
