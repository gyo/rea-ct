import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import * as React from "react";
import { IUser } from "../../stores/";
import ListItemLink from "../ListItemLink";

export interface IRsvUserCollectionProps {
  users?: IUser[];
}

const RsvUserCollection: React.FC<IRsvUserCollectionProps> = ({
  users = []
}) => (
  <>
    {users.length === 0 ? (
      <div>ユーザーが存在しません。</div>
    ) : (
      <List>
        {users.map(user => (
          <ListItemLink to={`/rsv-users/${user.id}/`} key={user.id}>
            <ListItemText>{user.name}</ListItemText>
          </ListItemLink>
        ))}
      </List>
    )}
  </>
);

export default RsvUserCollection;
