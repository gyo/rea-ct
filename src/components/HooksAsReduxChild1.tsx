import * as React from "react";
import * as action from "../hooksRedux/action";
import Context from "../hooksRedux/context";

const Component: React.FC = () => {
  const { state, dispatch } = React.useContext(Context);
  const [addCountValue, setAddCountValue] = React.useState(0);

  return (
    <>
      <h2>Hooks Redux Child1</h2>

      <div>
        <h3>{state.count}</h3>
        <button
          onClick={() => {
            dispatch(action.incrementCount({}));
          }}
        >
          INCREMENT
        </button>
        <button
          onClick={() => {
            dispatch(action.decrementCount({}));
          }}
        >
          DECREMENT
        </button>
      </div>

      <div>
        <h3>{state.value}</h3>
        <input
          type="text"
          value={state.value}
          onChange={e => {
            dispatch(action.updateValue({ value: e.target.value }));
          }}
        />
      </div>

      <div>
        <h3>{state.count}</h3>
        <input
          type="number"
          value={addCountValue}
          onChange={e => {
            const valueAsNumber = parseInt(e.target.value, 10);
            setAddCountValue(isNaN(valueAsNumber) ? 0 : valueAsNumber);
          }}
        />
        <button
          onClick={() => {
            dispatch(
              action.addCount({
                count: addCountValue
              })
            );
            setAddCountValue(0);
          }}
        >
          ADD
        </button>
      </div>
    </>
  );
};

export default Component;
