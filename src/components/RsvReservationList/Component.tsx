import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import * as React from "react";
import { IReservation } from "../../stores/";
import ListItemLink from "../ListItemLink";

export interface IRsvReservationCollectionProps {
  reservations?: IReservation[];
}

const RsvReservationCollection: React.FC<IRsvReservationCollectionProps> = ({
  reservations = []
}) => (
  <>
    {reservations.length === 0 ? (
      <div>予約が存在しません。</div>
    ) : (
      <List>
        {reservations.map(reservation => (
          <ListItemLink
            to={`/rsv-reservations/${reservation.id}/`}
            key={reservation.id}
          >
            <ListItemText>{`${reservation.dateTime.toLocaleDateString()} ${reservation.dateTime.toLocaleTimeString()}`}</ListItemText>
          </ListItemLink>
        ))}
      </List>
    )}
  </>
);

export default RsvReservationCollection;
