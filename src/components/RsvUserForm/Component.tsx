import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { History } from "history";
import * as React from "react";
import { useTextField } from "../../hooks";
import { IUser } from "../../stores/";

export interface IRsvUserFormProps {
  userId?: string;
  user?: IUser;
  mode?: "viewer" | "editor";
  submit?: (user: IUser) => void;
  submitText?: string;
  history?: History;
  transitionTo?: string;
}

const Component: React.FC<IRsvUserFormProps> = ({
  userId,
  user,
  mode = "editor",
  submit = () => {},
  submitText = "SUBMIT",
  history,
  transitionTo
}) => {
  const [name, updateName] = useTextField(user ? user.name : "");

  const isEditor = mode === "editor";

  return userId && !user ? (
    <div>対象ユーザーが存在しません。</div>
  ) : (
    <>
      <div style={{ padding: 16 }}>
        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField label="名前" value={name} onChange={updateName} />
          ) : (
            <>
              <Typography variant="caption" component="label">
                名前
              </Typography>

              <Typography>{user ? user.name : ""}</Typography>
            </>
          )}
        </div>

        {isEditor && (
          <div style={{ marginTop: 16 }}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              onClick={() => {
                submit({
                  id: userId || "",
                  name
                });
                if (history && transitionTo) {
                  history.push(transitionTo);
                }
              }}
            >
              {submitText}
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default Component;
