export const dateTimeLocalToString = (date: Date) => {
  const yearString: string = `${date.getFullYear()}`.padStart(4, "0");
  const monthString: string = `${date.getMonth() + 1}`.padStart(2, "0");
  const dateString: string = `${date.getDate()}`.padStart(2, "0");
  const hourString: string = `${date.getHours()}`.padStart(2, "0");
  const minutesString: string = `${date.getMinutes()}`.padStart(2, "0");

  return `${yearString}-${monthString}-${dateString}T${hourString}:${minutesString}`;
};

export const dateTimeLocalToDate = (dateTimeLocalString: string) => {
  const matched = /([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2})/.exec(
    dateTimeLocalString
  );

  if (matched === null) {
    throw new Error("dateTimeLocalString is not matched expected pattern.");
  }

  return new Date(
    `${matched[1]}-${matched[2]}-${matched[3]} ${matched[4]}:${matched[5]}`
  );
};
