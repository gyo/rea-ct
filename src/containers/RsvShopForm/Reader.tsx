import { connect } from "react-redux";
import RsvShopForm from "../../components/RsvShopForm";
import { IRsvShopFormProps } from "../../components/RsvShopForm/Component";
import { IStore } from "../../stores";
import { IShop } from "../../stores/";

interface IStoreProps {
  shop?: IShop;
}

export default connect<IStoreProps, {}, IRsvShopFormProps, IStore>(
  (state, props) => {
    const selectedShop = state.shops.find(shop => shop.id === props.shopId);

    return {
      shop: selectedShop
    };
  }
)(RsvShopForm);
