import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateShop } from "../../actions";
import RsvShopForm from "../../components/RsvShopForm";
import { IRsvShopFormProps } from "../../components/RsvShopForm/Component";
import { IStore } from "../../stores";
import { IShop } from "../../stores/";

interface IStoreProps {
  shop?: IShop;
}

interface IDispatchProps {
  submit: (shop: IShop) => void;
}

export default connect<IStoreProps, IDispatchProps, IRsvShopFormProps, IStore>(
  (state, props) => {
    const selectedShop = state.shops.find(shop => shop.id === props.shopId);

    return {
      shop: selectedShop
    };
  },
  dispatch =>
    bindActionCreators(
      {
        submit: shop => updateShop({ shop })
      },
      dispatch
    )
)(RsvShopForm);
