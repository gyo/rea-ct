import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createReservation } from "../../actions";
import RsvReservationForm from "../../components/RsvReservationForm";
import { IRsvReservationFormProps } from "../../components/RsvReservationForm/Component";
import { IStore } from "../../stores";
import { IMenu } from "../../stores/";
import { IReservation } from "../../stores/";
import { IUser } from "../../stores/";

interface IStoreProps {
  menus: IMenu[];
  users: IUser[];
}

interface IDispatchProps {
  submit: (reservation: IReservation) => void;
}

export default connect<
  IStoreProps,
  IDispatchProps,
  IRsvReservationFormProps,
  IStore
>(
  state => {
    return {
      menus: state.menus,
      users: state.users
    };
  },
  dispatch =>
    bindActionCreators(
      {
        submit: reservation => createReservation({ reservation })
      },
      dispatch
    )
)(RsvReservationForm);
