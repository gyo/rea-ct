import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateMenu } from "../../actions";
import RsvMenuForm from "../../components/RsvMenuForm";
import { IRsvMenuFormProps } from "../../components/RsvMenuForm/Component";
import { IStore } from "../../stores";
import { IMenu } from "../../stores/";
import { IShop } from "../../stores/";

interface IStoreProps {
  menu?: IMenu;
  shops: IShop[];
}

interface IDispatchProps {
  submit: (menu: IMenu) => void;
}

export default connect<IStoreProps, IDispatchProps, IRsvMenuFormProps, IStore>(
  (state, props) => {
    const selectedMenu = state.menus.find(menu => menu.id === props.menuId);

    return {
      menu: selectedMenu,
      shops: state.shops
    };
  },
  dispatch =>
    bindActionCreators(
      {
        submit: menu => updateMenu({ menu })
      },
      dispatch
    )
)(RsvMenuForm);
