import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createMenu } from "../../actions";
import RsvMenuForm from "../../components/RsvMenuForm";
import { IRsvMenuFormProps } from "../../components/RsvMenuForm/Component";
import { IStore } from "../../stores";
import { IMenu } from "../../stores/";
import { IShop } from "../../stores/";

interface IStoreProps {
  shops: IShop[];
}

interface IDispatchProps {
  submit: (menu: IMenu) => void;
}

export default connect<IStoreProps, IDispatchProps, IRsvMenuFormProps, IStore>(
  state => {
    return {
      shops: state.shops
    };
  },
  dispatch =>
    bindActionCreators(
      {
        submit: menu => createMenu({ menu })
      },
      dispatch
    )
)(RsvMenuForm);
