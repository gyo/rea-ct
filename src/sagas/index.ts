import { all, take } from "redux-saga/effects";

export function* watchAndLog() {
  while (true) {
    const action = yield take("*");
    console.log("[# saga logger] action is", action);
  }
}

export default function* rootSaga() {
  yield all([watchAndLog()]);
}
