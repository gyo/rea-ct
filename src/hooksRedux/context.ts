import * as React from "react";
import { Action } from "typescript-fsa";
import { initialState } from "../hooksRedux/store";
import { IAddCount, IUpdateValue } from "../hooksRedux/action";

const Context = React.createContext({
  state: initialState,
  dispatch: (i: Action<IAddCount | IUpdateValue | {}>) => {
    console.log(i);
  }
});

export default Context;
