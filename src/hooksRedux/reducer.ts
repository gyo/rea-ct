import { reducerWithInitialState } from "typescript-fsa-reducers";
import * as action from "./action";
import * as store from "./store";

const reducer = reducerWithInitialState(store.initialState)
  .case(action.incrementCount, state => {
    return {
      ...state,
      count: state.count + 1
    };
  })
  .case(action.decrementCount, state => {
    return {
      ...state,
      count: state.count - 1
    };
  })
  .case(action.addCount, (state, payload) => {
    return {
      ...state,
      count: state.count + payload.count
    };
  })
  .case(action.updateValue, (state, payload) => {
    return {
      ...state,
      value: payload.value
    };
  });

export default reducer;
