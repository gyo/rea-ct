import actionCreatorFactory from "typescript-fsa";

const actionCreator = actionCreatorFactory();

export const incrementCount = actionCreator<{}>("INCREMENT_COUNT");

export const decrementCount = actionCreator<{}>("DECREMENT_COUNT");

export interface IAddCount {
  count: number;
}
export const addCount = actionCreator<IAddCount>("ADD_COUNT");

export interface IUpdateValue {
  value: string;
}
export const updateValue = actionCreator<IUpdateValue>("UPDATE_VALUE");
