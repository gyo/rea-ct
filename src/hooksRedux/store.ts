export interface IStore {
  count: number;
  value: string;
}

export const initialState: IStore = {
  count: 0,
  value: "foo"
};
