export interface IUser {
  id: string;
  name: string;
}

export const users = [
  {
    id: "userId0",
    name: "ユーザー0"
  },
  {
    id: "userId1",
    name: "ユーザー1"
  },
  {
    id: "userId2",
    name: "ユーザー2"
  },
  {
    id: "userId3",
    name: "ユーザー3"
  },
  {
    id: "userId4",
    name: "ユーザー4"
  }
];
