export interface IReservableDateTime {
  startTime: Date;
  endTime: Date;
}

export interface IMenu {
  id: string;
  name: string;
  requiredMinutes: number;
  reservableDateTimes: IReservableDateTime[];
  resourceOccupancy: number;
  shopId: string;
}

export const menus = [
  {
    id: "menuId0",
    name: "メニュー0",
    requiredMinutes: 60,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 12:00"),
        startTime: new Date("2019-01-01 09:00")
      },
      {
        endTime: new Date("2019-01-01 18:00"),
        startTime: new Date("2019-01-01 13:00")
      },
      {
        endTime: new Date("2019-01-02 12:00"),
        startTime: new Date("2019-01-02 09:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 12:00"),
        startTime: new Date("2019-01-03 09:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      },
      {
        endTime: new Date("2019-01-04 12:00"),
        startTime: new Date("2019-01-04 09:00")
      },
      {
        endTime: new Date("2019-01-04 18:00"),
        startTime: new Date("2019-01-04 13:00")
      },
      {
        endTime: new Date("2019-01-05 12:00"),
        startTime: new Date("2019-01-05 09:00")
      },
      {
        endTime: new Date("2019-01-05 18:00"),
        startTime: new Date("2019-01-05 13:00")
      }
    ],
    resourceOccupancy: 100,
    shopId: "shopId0"
  },
  {
    id: "menuId1",
    name: "メニュー1",
    requiredMinutes: 30,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 12:00"),
        startTime: new Date("2019-01-01 09:00")
      },
      {
        endTime: new Date("2019-01-01 18:00"),
        startTime: new Date("2019-01-01 13:00")
      },
      {
        endTime: new Date("2019-01-02 12:00"),
        startTime: new Date("2019-01-02 09:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 12:00"),
        startTime: new Date("2019-01-03 09:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      }
    ],
    resourceOccupancy: 50,
    shopId: "shopId0"
  },
  {
    id: "menuId2",
    name: "メニュー2",
    requiredMinutes: 20,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-02 12:00"),
        startTime: new Date("2019-01-02 09:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 12:00"),
        startTime: new Date("2019-01-03 09:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      },
      {
        endTime: new Date("2019-01-04 12:00"),
        startTime: new Date("2019-01-04 09:00")
      },
      {
        endTime: new Date("2019-01-04 18:00"),
        startTime: new Date("2019-01-04 13:00")
      }
    ],
    resourceOccupancy: 33,
    shopId: "shopId0"
  },
  {
    id: "menuId3",
    name: "メニュー3",
    requiredMinutes: 15,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 18:00"),
        startTime: new Date("2019-01-01 13:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      },
      {
        endTime: new Date("2019-01-04 18:00"),
        startTime: new Date("2019-01-04 13:00")
      },
      {
        endTime: new Date("2019-01-05 18:00"),
        startTime: new Date("2019-01-05 13:00")
      }
    ],
    resourceOccupancy: 25,
    shopId: "shopId0"
  },
  {
    id: "menuId4",
    name: "メニュー4",
    requiredMinutes: 115,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 13:00"),
        startTime: new Date("2019-01-01 10:00")
      },
      {
        endTime: new Date("2019-01-01 17:00"),
        startTime: new Date("2019-01-01 14:00")
      },
      {
        endTime: new Date("2019-01-01 21:00"),
        startTime: new Date("2019-01-01 18:00")
      },
      {
        endTime: new Date("2019-01-02 13:00"),
        startTime: new Date("2019-01-02 10:00")
      },
      {
        endTime: new Date("2019-01-02 17:00"),
        startTime: new Date("2019-01-02 14:00")
      },
      {
        endTime: new Date("2019-01-02 21:00"),
        startTime: new Date("2019-01-02 18:00")
      },
      {
        endTime: new Date("2019-01-03 13:00"),
        startTime: new Date("2019-01-03 10:00")
      },
      {
        endTime: new Date("2019-01-03 17:00"),
        startTime: new Date("2019-01-03 14:00")
      },
      {
        endTime: new Date("2019-01-03 21:00"),
        startTime: new Date("2019-01-03 18:00")
      },
      {
        endTime: new Date("2019-01-04 13:00"),
        startTime: new Date("2019-01-04 10:00")
      },
      {
        endTime: new Date("2019-01-04 17:00"),
        startTime: new Date("2019-01-04 14:00")
      },
      {
        endTime: new Date("2019-01-04 21:00"),
        startTime: new Date("2019-01-04 18:00")
      }
    ],
    resourceOccupancy: 100,
    shopId: "shopId0"
  },
  {
    id: "menuId5",
    name: "メニュー5",
    requiredMinutes: 60,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 12:00"),
        startTime: new Date("2019-01-01 09:00")
      },
      {
        endTime: new Date("2019-01-01 18:00"),
        startTime: new Date("2019-01-01 13:00")
      },
      {
        endTime: new Date("2019-01-02 12:00"),
        startTime: new Date("2019-01-02 09:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 12:00"),
        startTime: new Date("2019-01-03 09:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      },
      {
        endTime: new Date("2019-01-04 12:00"),
        startTime: new Date("2019-01-04 09:00")
      },
      {
        endTime: new Date("2019-01-04 18:00"),
        startTime: new Date("2019-01-04 13:00")
      },
      {
        endTime: new Date("2019-01-05 12:00"),
        startTime: new Date("2019-01-05 09:00")
      },
      {
        endTime: new Date("2019-01-05 18:00"),
        startTime: new Date("2019-01-05 13:00")
      }
    ],
    resourceOccupancy: 100,
    shopId: "shopId1"
  },
  {
    id: "menuId6",
    name: "メニュー6",
    requiredMinutes: 30,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 12:00"),
        startTime: new Date("2019-01-01 09:00")
      },
      {
        endTime: new Date("2019-01-01 18:00"),
        startTime: new Date("2019-01-01 13:00")
      },
      {
        endTime: new Date("2019-01-02 12:00"),
        startTime: new Date("2019-01-02 09:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 12:00"),
        startTime: new Date("2019-01-03 09:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      }
    ],
    resourceOccupancy: 50,
    shopId: "shopId1"
  },
  {
    id: "menuId7",
    name: "メニュー7",
    requiredMinutes: 20,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-02 12:00"),
        startTime: new Date("2019-01-02 09:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 12:00"),
        startTime: new Date("2019-01-03 09:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      },
      {
        endTime: new Date("2019-01-04 12:00"),
        startTime: new Date("2019-01-04 09:00")
      },
      {
        endTime: new Date("2019-01-04 18:00"),
        startTime: new Date("2019-01-04 13:00")
      }
    ],
    resourceOccupancy: 33,
    shopId: "shopId1"
  },
  {
    id: "menuId8",
    name: "メニュー8",
    requiredMinutes: 15,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 18:00"),
        startTime: new Date("2019-01-01 13:00")
      },
      {
        endTime: new Date("2019-01-02 18:00"),
        startTime: new Date("2019-01-02 13:00")
      },
      {
        endTime: new Date("2019-01-03 18:00"),
        startTime: new Date("2019-01-03 13:00")
      },
      {
        endTime: new Date("2019-01-04 18:00"),
        startTime: new Date("2019-01-04 13:00")
      },
      {
        endTime: new Date("2019-01-05 18:00"),
        startTime: new Date("2019-01-05 13:00")
      }
    ],
    resourceOccupancy: 25,
    shopId: "shopId1"
  },
  {
    id: "menuId9",
    name: "メニュー9",
    requiredMinutes: 115,
    reservableDateTimes: [
      {
        endTime: new Date("2019-01-01 13:00"),
        startTime: new Date("2019-01-01 10:00")
      },
      {
        endTime: new Date("2019-01-01 17:00"),
        startTime: new Date("2019-01-01 14:00")
      },
      {
        endTime: new Date("2019-01-01 21:00"),
        startTime: new Date("2019-01-01 18:00")
      },
      {
        endTime: new Date("2019-01-02 13:00"),
        startTime: new Date("2019-01-02 10:00")
      },
      {
        endTime: new Date("2019-01-02 17:00"),
        startTime: new Date("2019-01-02 14:00")
      },
      {
        endTime: new Date("2019-01-02 21:00"),
        startTime: new Date("2019-01-02 18:00")
      },
      {
        endTime: new Date("2019-01-03 13:00"),
        startTime: new Date("2019-01-03 10:00")
      },
      {
        endTime: new Date("2019-01-03 17:00"),
        startTime: new Date("2019-01-03 14:00")
      },
      {
        endTime: new Date("2019-01-03 21:00"),
        startTime: new Date("2019-01-03 18:00")
      },
      {
        endTime: new Date("2019-01-04 13:00"),
        startTime: new Date("2019-01-04 10:00")
      },
      {
        endTime: new Date("2019-01-04 17:00"),
        startTime: new Date("2019-01-04 14:00")
      },
      {
        endTime: new Date("2019-01-04 21:00"),
        startTime: new Date("2019-01-04 18:00")
      }
    ],
    resourceOccupancy: 100,
    shopId: "shopId2"
  }
];
